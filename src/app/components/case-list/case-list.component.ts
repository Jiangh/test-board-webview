import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddCaseComponent } from '../add-case/add-case.component';
import { StepConfig } from '../../model/step-config.model';

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.css']
})
export class CaseListComponent implements OnInit {

  @Input()
  caseName = 'Untitled';

  @Output()
  changeCase = new EventEmitter<StepConfig>();

  editMode = false;
  editButton = 'edit';
  selectedCase = '';

  cases = [];
  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  addCase() {
    const dialog = this.dialog.open(AddCaseComponent, {
      data: {
        title: '新建测试',
      }
    });
    dialog.afterClosed().subscribe(title => {
      if (title !== undefined && title !== '') {
        this.cases.push({ name: title, config: [] });
      }
    });
  }

  deleteCase(index) {
    this.cases = this.cases.filter((_, i) => i !== index);
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
  }

  editItem(item) {
    console.log(item);
    const dialog = this.dialog.open(AddCaseComponent, {
      data: {
        title: '编辑',
      }
    });
    dialog.afterClosed().subscribe(title => {
      if (title !== undefined && title !== '') {
        item.name = title;
      }
    });
  }

  clickItem(item) {
    this.selectedCase = item.name;
    this.changeCase.emit(item);
  }

}
