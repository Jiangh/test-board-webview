import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket/socket.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  private message = '123132131';
  // private connection;

  logs = [
    '2018/07/02 15:55 click!',
  ];

  constructor(private socketService: SocketService) {
    this.socketService.connect();
  }

  ngOnInit() {
    this.socketService.getMessager().pipe(
      catchError((err, caught) => {
        console.log(err);
        return caught;
      }),
    ).subscribe((message: any) => {
      this.logs.push(message);
    });
  }

  sendMessage() {
    console.log('send');
    this.socketService.sendMessage(this.message);
  }

}
