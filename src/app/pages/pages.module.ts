import { NgModule } from '@angular/core';
import { MainComponent } from './main/main.component';
import { SystemComponent } from './system/system.component';
import { ShareModule } from '../share/share.module';
import { ComponentsModule } from '../components/components.module';
import { ServicesModule } from '../services/services.module';

@NgModule({
  imports: [
    ShareModule,
    ComponentsModule,
    ServicesModule,
  ],
  declarations: [
    MainComponent,
    SystemComponent,
  ]
})
export class PagesModule { }
