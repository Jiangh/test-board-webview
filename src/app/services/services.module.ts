import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { GetConfigService } from './get-config/get-config.service';
import { SocketService } from './socket/socket.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    GetConfigService,
    SocketService,
  ],
  declarations: []
})
export class ServicesModule { }
