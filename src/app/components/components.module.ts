import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { ScenarioComponent } from './scenario/scenario.component';
import { LogComponent } from './log/log.component';
import { CaseListComponent } from './case-list/case-list.component';
import { AddStepComponent } from './add-step/add-step.component';
import { ShareModule } from '../share/share.module';
import { AddCaseComponent } from './add-case/add-case.component';
import { ServicesModule } from '../services/services.module';

@NgModule({
  imports: [
    ShareModule,
    ServicesModule,
  ],
  declarations: [
    HeaderComponent,
    ScenarioComponent,
    LogComponent,
    CaseListComponent,
    AddStepComponent,
    AddCaseComponent,
  ],
  exports: [
    HeaderComponent,
    ScenarioComponent,
    LogComponent,
    CaseListComponent,
    AddStepComponent,
    AddCaseComponent,
  ],
  entryComponents: [
    AddStepComponent,
    AddCaseComponent,
  ],
})
export class ComponentsModule { }
