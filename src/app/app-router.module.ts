import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { MainComponent } from './pages/main/main.component';
import { SystemComponent } from './pages/system/system.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: 'sys', component: SystemComponent },
  // { path: '**', redirectTo: 'main' },
  { path: '**', redirectTo: 'main' },

  // { path: 'path/:routeParam', component: MyComponent },
  // { path: 'staticPath', component: ... },
  // { path: '**', component: ... },
  // { path: 'oldPath', redirectTo: '/staticPath' },
  // { path: ..., component: ..., data: { message: 'Custom' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
