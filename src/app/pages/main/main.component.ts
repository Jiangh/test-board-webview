import { Component, OnInit, ViewChild, HostListener } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  scenario = 'hidden';
  functionConfig = [];
  caseName = '';

  constructor() { }

  ngOnInit() {
  }

  onChangeCase(functionConfig) {
    // console.log(functionConfig);
    this.functionConfig = functionConfig;
    this.scenario = 'visible';
  }

  onSave() {
    console.log('export:', this.functionConfig);

  }

  onSubmit(name) {
    this.caseName = name;
  }

  @HostListener('contextmenu', ['$event'])
  preventContextmenu(event: Event) {
    event.preventDefault();
  }


}
