export interface StepConfig {
  [key: string]: any;
  title?: string;
  stepNo?: number;
  action?: string;
  mode?: string;
  target?: string;
  value?: string;
}
