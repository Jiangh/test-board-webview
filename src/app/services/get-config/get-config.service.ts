import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetConfigService {

  constructor(private http: HttpClient) {
  }

  get actions() {
    return this.http.get('http://127.0.0.1:8000/api/get_actions').pipe(
      catchError(err => of([]))
    );
  }

  get modes() {
    return this.http.get('http://127.0.0.1:8000/api/get_modes').pipe(
      catchError(err => of([]))
    );
  }

  makeConfigInfo(cases) {
    this.http.post('http://127.0.0.1:8000/api/save_config', cases);
  }

}
